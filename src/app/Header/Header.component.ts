import { ViewChild } from '@angular/core';
import { Component, Inject, OnInit } from '@angular/core';
import { ModalContainerComponent, ModalDirective } from 'angular-bootstrap-md';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../shared/services/authentication.service';
import { User } from '../models/user.models';
import { AuthService } from '../shared/services/auth.service';

import { CookieService } from 'ngx-cookie';
import { Router } from '@angular/router';

import { ApiZeely } from '../shared/services/apiZeely.service';

@Component({
  selector: 'app-Header',
  templateUrl: './Header.component.html',
  styleUrls: ['./Header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @ViewChild('login', { static: true })
  login!: ModalContainerComponent;
  user = new User();

  @ViewChild('register', { static: true })
  register!: ModalContainerComponent;

  validatingForm: any;
  registerForm: any;
  authenticationService: AuthenticationService = new AuthenticationService();
  isAuth: boolean = false;
  error: any;

  constructor(
    public authService: AuthService,
    public cookieConnexion: CookieService,
    public router: Router,
    private api: ApiZeely
  ) {
    this.registerForm = new FormGroup({
      registerFormModalEmail: new FormControl('', Validators.email),
      registerFormModalPassword: new FormControl('', Validators.required),
      registerFormModalPassword2: new FormControl('', Validators.required),
      registerFormModalLastName: new FormControl('', Validators.required),
      registerFormModalFirstName: new FormControl('', Validators.required),
    });

    this.validatingForm = new FormGroup({
      loginFormModalEmail: new FormControl('', Validators.email),
      loginFormModalPassword: new FormControl('', Validators.required),
    });
  }
  ngOnInit(): void {
    this.authService.allUser();
  }

  get loginFormModalEmail() {
    return this.validatingForm.get('loginFormModalEmail');
  }

  get loginFormModalPassword() {
    return this.validatingForm.get('loginFormModalPassword');
  }

  get registerFormModalEmail() {
    return this.registerForm.get('registerFormModalEmail');
  }

  get registerFormModalPassword() {
    return this.registerForm.get('registerFormModalPassword');
  }

  get registerFormModalPassword2() {
    return this.registerForm.get('registerFormModalPassword2');
  }

  get registerFormModalUsername() {
    return this.registerForm.get('registerFormModalUsername');
  }

  get registerFormModalLastName() {
    return this.registerForm.get('registerFormModalLastName');
  }

  get registerFormModalFirstName() {
    return this.registerForm.get('registerFormModalFirstName');
  }

  onInscription() {
    const email: string = this.registerForm.get('registerFormModalEmail').value;
    const password: string = this.registerForm.get(
      'registerFormModalPassword'
    ).value;
    const password2: string = this.registerForm.get(
      'registerFormModalPassword2'
    ).value;
    const lastName: string = this.registerForm.get(
      'registerFormModalLastName'
    ).value;
    const firstName: string = this.registerForm.get(
      'registerFormModalFirstName'
    ).value;

    if (password === password2) {
      this.api
        .signUpUser(email, password, lastName, firstName)
        .subscribe((data: any) => {});

      this.register.hide();
    }
  }

  onConnexion() {
    const email: string = this.validatingForm.get('loginFormModalEmail').value;
    const password: string = this.validatingForm.get(
      'loginFormModalPassword'
    ).value;

    let isValidUser: Promise<Boolean> = this.authService.SignIn(this.user);

    if (isValidUser) {
      this.login.hide();
    } else console.warn('error');
  }

  SignOut() {
    this.authenticationService.SignOut();
    this.registerForm = new FormGroup({
      registerFormModalEmail: new FormControl('', Validators.email),
      registerFormModalPassword: new FormControl('', Validators.required),
      registerFormModalPassword2: new FormControl('', Validators.required),
      registerFormModalLastName: new FormControl('', Validators.required),
      registerFormModalFirstName: new FormControl('', Validators.required),
    });

    this.validatingForm = new FormGroup({
      loginFormModalEmail: new FormControl('', Validators.email),
      loginFormModalPassword: new FormControl('', Validators.required),
    });

    this.authService.logout();
    this.router.navigate(['']);
  }

  redirectTo() {
    this.router.navigate(['/gestion']);
  }

  myAccount() {
    this.router.navigate(['/MyAccount']);
  }
}
