import { Component, OnInit } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { CookieService } from 'ngx-cookie';
import { ApiZeely } from '../shared/services/apiZeely.service';

@Component({
  selector: 'app-gestion',
  templateUrl: './gestion.component.html',
  styleUrls: ['./gestion.component.scss'],
})
export class GestionComponent implements OnInit {
  Orders: any = [];
  tempOrders: any = [];
  headElements: any = [
    'Numéro de commande',
    'Utilisateur',
    'Libelle',
    'Description',
    'Prix',
  ];

  constructor(private api: ApiZeely, private cookieOrders: CookieService) {
    var concert: any = [];
    this.api.Orders().subscribe((data: any) => {
      this.tempOrders = data;
      this.tempOrders.forEach((billet: any, index: any) => {
        this.api.ConcertId(billet['idConcert']).subscribe((concert: any) => {
          this.tempOrders[index]['libelle'] = concert[0]['libelle'];
          this.tempOrders[index]['description'] = concert[0]['description'];
          this.tempOrders[index]['prix'] = concert[0]['prix'];
        });
        this.Orders = this.tempOrders;
      });
    });
  }

  ngOnInit() {}
}
