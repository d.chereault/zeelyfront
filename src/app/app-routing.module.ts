import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './Accueil/Accueil.component';
import { CompteComponent } from './compte/compte.component';
import { GestionComponent } from './gestion/gestion.component';
import { PanelAdminGuard } from './panel-admin.guard';
import { ShopComponent } from './shop/shop.component';

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'Commande', component: ShopComponent },
  {
    path: 'gestion',
    component: GestionComponent,
    canActivate: [PanelAdminGuard],
  },
  { path: 'MyAccount', component: CompteComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
