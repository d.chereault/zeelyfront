import { TestBed } from '@angular/core/testing';

import { PanelAdminGuard } from './panel-admin.guard';

describe('PanelAdminGuard', () => {
  let guard: PanelAdminGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(PanelAdminGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
