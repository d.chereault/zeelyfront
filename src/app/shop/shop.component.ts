import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PanierService } from '../shared/services/panier.service';

import { CookieService } from 'ngx-cookie';
import { ApiZeely } from '../shared/services/apiZeely.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],
})
export class ShopComponent implements OnInit {
  headElements: any = ['Libelle', 'Description', 'Prix'];
  command: any = [];
  panier: any = [];

  constructor(
    private route: Router,
    private monPanier: PanierService,
    public cookiePanier: CookieService,
    private api: ApiZeely
  ) {}

  ngOnInit() {
    this.panier = this.monPanier.getPanier();
    if (this.cookiePanier.getObject('shop') != null) {
      this.panier = this.cookiePanier.getObject('shop');
    }
  }

  buy() {
    if (this.cookiePanier.get('isloggedIn') == 'true') {
      this.panier.forEach((concert: any) => {
        let idUser = this.api.getIdCurrentUser(
          this.cookiePanier.get('loggedUser')
        );

        let numero: string = concert['Id'] + '' + new Date().getTime() + '';

        this.api
          .booking(
            numero,
            this.cookiePanier.get('loggedUser'),
            Number(concert['Id'])
          )
          .subscribe((data: any) => {});
      });
      this.cookiePanier.remove('shop');
      this.route.navigate(['']);
    } else {
    }
  }
}
