import { Injectable, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.models';
import { ApiZeely } from './apiZeely.service';

import { CookieService } from 'ngx-cookie';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  listOfUser: any = [];
  users: User[] = [];

  public loggedUser!: string;
  public isloggedIn!: Boolean;
  public roles!: string[];
  constructor(private api: ApiZeely, private cookieConnexion: CookieService) {}

  allUser() {}

  logout() {
    this.isloggedIn = false;
    this.roles = [];
    this.cookieConnexion.remove('loggedUser');
    this.cookieConnexion.remove('isOP');
    this.cookieConnexion.put('isloggedIn', String(this.isloggedIn), {
      //httpOnly: true,
    });
  }

  async SignIn(user: User): Promise<Boolean> {
    let validUser: Boolean = false;

    const data: any = this.api
      .usersCall(user.email, user.password)
      .subscribe((data: any) => {
        console.warn(data);
        validUser = true;
        this.isloggedIn = true;
        this.cookieConnexion.put('loggedUser', String(user.email), {
          //httpOnly: true,
          expires: new Date(new Date().getTime() + 5400000),
        });
        this.cookieConnexion.put('isloggedIn', String(this.isloggedIn), {
          //httpOnly: true,
          expires: new Date(new Date().getTime() + 5400000),
        });
        console.warn(data['roles']);
        console.warn(data['roles'].indexOf('ADMIN'));
        if (data['roles'].indexOf('ADMIN') > -1) {
          this.cookieConnexion.put('isOP', 'true', {
            //httpOnly: true,
            expires: new Date(new Date().getTime() + 5400000),
          });
        }
      });

    return validUser;
  }

  isAdmin(): Boolean {
    if (!this.roles) {
      return false;
    } else {
      return this.roles.indexOf('ADMIN') > -1;
    }
  }
}
