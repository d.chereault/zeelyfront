import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PanierService {
  panier: any = [];
  constructor() {}

  getPanier() {
    return this.panier;
  }
  setPanier(panier: any) {
    this.panier = panier;
  }
}
