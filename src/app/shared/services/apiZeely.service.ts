import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user.models';

@Injectable({
  providedIn: 'root',
})
export class ApiZeely {
  private url: string = 'http://127.0.0.1:8000/';

  constructor(private http: HttpClient) {}

  concertsCall() {
    return this.http.get(this.url + 'api/concerts.json');
  }

  idConcert(id: number) {
    return this.http.get(this.url + 'api/concerts/' + id + '.json');
  }

  usersCall(email: string, password: string): Observable<any> {
    var toJson = {
      username: email,
      password: password,
    };

    const headers = { 'content-type': 'application/json' };
    const body = JSON.stringify(toJson);

    return this.http.post(this.url + 'api/login', body, {
      headers: headers,
    });
  }

  signUpUser(
    email: string,
    password: string,
    lastName: string,
    firstname: string
  ): Observable<any> {
    var toJson = {
      username: email,
      roles: ['user'],
      password: password,
      FirstName: firstname,
      LastName: lastName,
      Adresse: null,
      DDN: null,
      firstName: firstname,
      lastName: lastName,
      adresse: null,
      dDN: null,
    };

    const body = JSON.stringify(toJson);
    const headers = {
      'content-type': 'application/json',
      'content-length': String(body.length),
    };
    console.warn(String(body.length));

    return this.http.post(this.url + 'api/users', body, {
      headers: headers,
    });
  }

  booking(Numero: string, idUser: string, idConcert: number): Observable<any> {
    var toJson = {
      numero: Numero,
      idUser: idUser,
      idConcert: idConcert,
    };

    const headers = { 'content-type': 'application/json' };
    const body = JSON.stringify(toJson);

    return this.http.post(this.url + 'api/orders.json', body, {
      headers: headers,
    });
  }

  getIdCurrentUser(email: string) {
    var listOfUser: any = [];

    this.http.get(this.url + 'api/users.json').subscribe((data) => {
      listOfUser = data;

      listOfUser.forEach((person: any) => {
        if (person['email'] == email) {
          return person['id'];
        }
      });
    });
  }

  myOrders(email: string) {
    return this.http.get(this.url + 'api/orders.json?idUser=' + email);
  }

  Orders() {
    return this.http.get(this.url + 'api/orders.json');
  }

  ConcertId(id: number) {
    return this.http.get(this.url + 'api/concerts.json?id=' + id);
  }

  OrderToUser(email: number) {
    return this.http.get(this.url + 'api/users.json?email=' + email);
  }

  restofPlace(idConcert: string) {
    return this.http.get(this.url + 'api/orders?idConcert=' + idConcert);
  }
}
