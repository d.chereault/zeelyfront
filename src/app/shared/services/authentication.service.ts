import { Injectable } from '@angular/core';
import firebase from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor() {}

  public signUpUser(email: string, password: string) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(() => {
          console.log('connecté');
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

  /* Sign in */

  public SignIn(email: string, password: string) {
    return new Promise((resolve, reject) => {
      firebase
        .auth()
        .signInWithEmailAndPassword(email, password)
        .then(() => {
          console.log("connecté, c'est bon");
        })
        .catch((error) => {
          console.log('Something went wrong:', error.message);
        });
    });
  }

  /* Sign out */
  SignOut() {
    firebase.auth().signOut();
  }
}
