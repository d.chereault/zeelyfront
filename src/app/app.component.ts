import { Component } from '@angular/core';
import firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'Zeely';

  firebaseConfig = {
    apiKey: 'AIzaSyD0PvwZxXhydwfTL3iZ6O71KNlmFPwbVRs',
    authDomain: 'zeely-65597.firebaseapp.com',
    databaseURL: 'https://zeely-65597-default-rtdb.firebaseio.com',
    projectId: 'zeely-65597',
    storageBucket: 'zeely-65597.appspot.com',
    messagingSenderId: '272226204233',
    appId: '1:272226204233:web:db35447ea5b080b4f11d98',
    measurementId: 'G-R9ELXNRGYT',
  };

  constructor() {
    firebase.initializeApp(this.firebaseConfig);
  }
}
