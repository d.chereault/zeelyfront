import { Component, OnInit } from '@angular/core';
import { CookieModule, CookieService } from 'ngx-cookie';
import { ApiZeely } from '../shared/services/apiZeely.service';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.scss'],
})
export class CompteComponent implements OnInit {
  myOrders?: any = [];
  tempMyOrders?: any = [];
  headElements: any = ['Numéro', 'Libelle', 'Description', 'Prix'];
  timeLeft: number = 20;
  interval: any;

  constructor(private api: ApiZeely, private cookieOrders: CookieService) {
    var concert: any = [];
    this.api
      .myOrders(this.cookieOrders.get('loggedUser'))
      .subscribe((data: any) => {
        this.tempMyOrders = data;
        this.tempMyOrders.forEach((billet: any, index: any) => {
          this.api.ConcertId(billet['idConcert']).subscribe((concert: any) => {
            this.tempMyOrders[index]['libelle'] = concert[0]['libelle'];
            this.tempMyOrders[index]['description'] = concert[0]['description'];
            this.tempMyOrders[index]['prix'] = concert[0]['prix'];
          });
          this.myOrders = this.tempMyOrders;
        });
      });
  }

  ngOnInit() {}
}
