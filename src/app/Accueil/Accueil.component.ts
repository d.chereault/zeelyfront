import { Component, Injectable, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Concert } from '../models/concert.models';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ApiZeely } from '../shared/services/apiZeely.service';
import { PanierService } from '../shared/services/panier.service';

import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-Accueil',
  templateUrl: './Accueil.component.html',
  styleUrls: ['./Accueil.component.scss'],
})
export class AccueilComponent {
  concerts: any = [];
  data: any = [];
  panier: any = [];
  jsonConcert: any = [];
  isCartNull: boolean = true;
  tempConcerts: any = [];

  constructor(
    private api: ApiZeely,
    private monPanier: PanierService,
    private httpClient: HttpClient,
    private router: Router,
    public cookiePanier: CookieService
  ) {}

  ngOnInit() {
    this.api.concertsCall().subscribe((data: any) => {
      this.tempConcerts = data;
      this.tempConcerts.forEach((concert: any, numero: number) => {
        this.api.restofPlace(concert['id']).subscribe((place: any) => {
          this.concerts[numero].remainingPlaces = String(
            Number(concert['nbPlaces']) - Number(place['hydra:totalItems'])
          );
        });
        this.concerts = this.tempConcerts;
      });
      console.warn(this.concerts);
    });

    if (this.cookiePanier.getObject('shop') != null) {
      this.panier = this.cookiePanier.getObject('shop');
    }
  }

  addPanier(id: number) {
    this.isCartNull = false;
    this.api.idConcert(id).subscribe((data) => {
      this.jsonConcert = data;
      var concert: Concert = new Concert(
        this.jsonConcert['id'],
        this.jsonConcert['id_artiste'],
        this.jsonConcert['libelle'],
        this.jsonConcert['description'],
        this.jsonConcert['nb_places'],
        new Date(this.jsonConcert['date']),
        this.jsonConcert['image'],
        this.jsonConcert['prix']
      );
      this.panier.push(concert);
      this.updatePanier();
    });
  }

  updatePanier() {
    this.cookiePanier.remove('shop');
    var tempDate = new Date().getTime();
    tempDate = tempDate + 1200000;
    var expires = new Date(tempDate);
    this.cookiePanier.putObject('shop', this.panier, {
      expires: expires,
    });
    if (this.panier.length == 0) {
      this.cookiePanier.remove('shop');
      this.isCartNull = true;
    }
  }

  onDelete(id: number) {
    this.panier.splice(id, 1);
    this.updatePanier();
  }

  getCommand() {
    return this.cookiePanier.getObject('shop');
  }

  onCommande() {
    this.monPanier.setPanier(this.panier);
    this.router.navigate(['/Commande']);
  }

  toContinue() {
    if (
      this.cookiePanier.get('isloggedIn') == 'true' &&
      this.isCartNull == false
    ) {
      return false;
    } else return true;
  }

  /*CreateConcert() {
    let concert: any;
    concert['title'] = this.title;
    concert['artiste'] = this.artiste;
    concert['description'] = this.description;
    concert['photo'] = this.photo;

    this.concertService.createConcert(concert).then((res) => {
      this.artiste = '';
      this.title = '';
      this.photo = '';
      this.description = '';
    });
  }
  */
}
