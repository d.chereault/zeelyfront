import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { AccueilComponent } from './Accueil/Accueil.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './Header/Header.component';
import { ShopComponent } from './shop/shop.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

//Firebase Module
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

//Service
import { ApiZeely } from './shared/services/apiZeely.service';

//Angular to PHP
import { HttpClientModule } from '@angular/common/http';

//Cookie
import { CookieModule } from 'ngx-cookie';
import { GestionComponent } from './gestion/gestion.component';
import { CompteComponent } from './compte/compte.component';

@NgModule({
  declarations: [
    AppComponent,
    AccueilComponent,
    FooterComponent,
    HeaderComponent,
    ShopComponent,
    GestionComponent,
    CompteComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    HttpClientModule,
    CookieModule.forRoot(),
  ],
  providers: [AngularFirestore, ApiZeely],
  bootstrap: [AppComponent],
})
export class AppModule {}
