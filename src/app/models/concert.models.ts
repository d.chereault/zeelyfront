export class Concert {
  public Id: number;
  public IdArtiste: number;
  public libelle: string;
  public description: string;
  public nbPlaces: string;
  public dateConcert: Date;
  public images: string;
  public prix: number;

  constructor(
    Id: number,
    IdArtiste: String,
    libelle: string,
    description: string,
    nbPlaces: string,
    dateConcert: Date,
    images: string,
    prix: number
  ) {
    this.Id = Id;
    this.IdArtiste = Id;
    this.libelle = libelle;
    this.description = description;
    this.nbPlaces = nbPlaces;
    this.dateConcert = dateConcert;
    this.images = images;
    this.prix = prix;
  }
}
